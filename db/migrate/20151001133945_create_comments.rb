class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :comment
      t.references :post, index: true
      t.integer :user_id
      t.datetime :deleted_at
    end
  end
end
