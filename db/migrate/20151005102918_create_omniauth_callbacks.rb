class CreateOmniauthCallbacks < ActiveRecord::Migration
  def change
    create_table :omniauth_callbacks do |t|

      t.timestamps null: false
    end
  end
end
