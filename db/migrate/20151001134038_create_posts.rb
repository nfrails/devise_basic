class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.integer :user_id
      t.json :avatars
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
