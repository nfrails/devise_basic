module ApplicationHelper
  def is_following?(selected_author_id)
    return User.is_following(current_user.id).exists?(user_id: selected_author_id)
  end
end
