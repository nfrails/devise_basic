class User < ActiveRecord::Base
  include Gravtastic
  gravtastic

  has_many :posts
  has_many :comments
  has_many :followers, class_name: 'Followings', foreign_key: 'user_id'
  has_many :following, class_name: 'Followings', foreign_key: 'follower_id'
  devise :omniauthable, :omniauth_providers => [:google_oauth2, :twitter, :facebook, :linkedin, :github]

  scope :email_finding, ->(user_id) { find(user_id).email}

  scope :all_except, ->(current_user_id) { where.not(id: current_user_id)}

  scope :is_following, ->(current_user_id) { find(current_user_id).following}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def self.find_for_oauth(access_token, signed_in_resource=nil)
    data = access_token.info

    if access_token.provider == "twitter"
      user = User.where(:email => "#{data["nickname"]}@twitter.com").first
    else
      user = User.where(:email => data["email"]).first
    end


    # users to be created if they don't exist
    if user.blank?
      user = User.new(email: data["email"],
                      password: Devise.friendly_token[0,20]
      )
      if access_token.provider == "twitter"
        user.email = "#{data["nickname"]}@twitter.com"
        user.save
      else
        user.save!
      end
    end
    user
  end
end
