class Post < ActiveRecord::Base
  has_many :post_attachments

  accepts_nested_attributes_for :post_attachments
  #mount_uploaders :avatars, AvatarUploader

  acts_as_paranoid

  belongs_to :user
  has_many :comments, dependent: :destroy

end
