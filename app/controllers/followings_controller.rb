class FollowingsController < ApplicationController
  before_action :set_following, only: [:show, :edit, :update, :destroy]

  # GET /followings
  # GET /followings.json
  def index
    @following = current_user.following.count
    @followers = current_user.followers.count
  end

  def follow
    #@following = Following.new(following_params)
    @following = current_user.following.create user_id: params[:id]
    Blogmail.send_email(User.find(params[:id]).email,current_user.email).deliver
    @author_id = params[:id]
    #@action = 'follow'
    @notice = 'success'

  end

  # DELETE /followings/1
  # DELETE /followings/1.json
  def destroy
    @author_id = @following.user_id
    @following.destroy

    #@current_user.following.where(user_id: params[:user_id]).first.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_following
      @following = Followings.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def following_params
      params[:following]
    end
end
