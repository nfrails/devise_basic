class UsersController < ApplicationController
  def index
    @posts = []
    current_user.following.each do |following|
      @posts += following.user.posts
    end
    @following = current_user.following.count
    @followers = current_user.followers.count
  end

  def authors
    @authors = User.all_except(current_user.id)
  end

  def posts
    @posts = Post.where(user_id: params[:id])
    @author_id = params[:id]
    if current_user.following.exists?(user_id: @author_id)
      @followed = true
      @following = current_user.following.where(user_id: @author_id).first
    else
      @followed = false
    end
    #render template: 'posts/index'
  end

end
