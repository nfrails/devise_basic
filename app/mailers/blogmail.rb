class Blogmail < ApplicationMailer
  def send_email(email,follower_email)
    @follower_name = follower_email
    mail(to: email, subject: 'A user followed you')
  end
end
